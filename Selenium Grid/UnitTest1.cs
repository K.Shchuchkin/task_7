using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using System;
using Xunit;

namespace Selenium_Grid
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            DriverOptions capabilities = new ChromeOptions();
            capabilities.AcceptInsecureCertificates = true;
            RemoteWebDriver RemoteBrowser = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), capabilities);
            RemoteBrowser.Manage().Window.Maximize();
            RemoteBrowser.Navigate().GoToUrl("http://github.com");
            RemoteBrowser.Quit();
            capabilities = new FirefoxOptions();
            capabilities.AcceptInsecureCertificates = true;
            RemoteBrowser = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), capabilities.ToCapabilities());
            RemoteBrowser.Manage().Window.Maximize();
            RemoteBrowser.Navigate().GoToUrl("http://github.com");
            RemoteBrowser.Quit();
        }
    }
}
